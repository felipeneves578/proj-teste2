import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 1, nome: 'Mr. Nice' },
  { id: 2, nome: 'Narco' },
  { id: 3, nome: 'Bombasto' },
  { id: 4, nome: 'Celeritas' },
  { id: 5, nome: 'Magneta' },
  { id: 6, nome: 'RubberMan' },
  { id: 7, nome: 'Dynama' },
  { id: 8, nome: 'Dr IQ' },
  { id: 9, nome: 'Magma' },
  { id: 10, nome: 'Tornado' }
];